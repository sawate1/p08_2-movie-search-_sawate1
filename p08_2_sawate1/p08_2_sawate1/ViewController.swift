//
//  ViewController.swift
//  p08_2_sawate1
//
//  Created by Shivani Awate on 5/10/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tlt: UILabel!
    @IBOutlet weak var plt: UITextView!

    @IBOutlet weak var plotTxt: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var ActorLabel: UILabel!
    @IBOutlet weak var searchField: UITextField!
    var actor : String!
    var titl : String!
    var poster_url : String!
    var img:UIImageView!
    //@IBOutlet var title: UILabel!
    @IBAction func didTapSearch(_ sender: Any) {
        ActorLabel.isHidden = false;
        let n = searchField.text
        let city = n?.replacingOccurrences(of: " ", with: "+")
        
        let urlString = "http://www.omdbapi.com/?t=\(city!)"
        print(urlString)
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error)
            } else {
                do {
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
                     self.titl = parsedData["Title"] as! String!
                   
                    
                   // print(currentConditions)
                    
                    self.actor = parsedData["Actors"] as! String!
                    self.poster_url = parsedData["Poster"] as! String!
                    print("........",self.poster_url)
                    let plot = parsedData["Plot"] as! String!
                   
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                          print("----",self.poster_url)
                        let ur = URL(string: self.poster_url)
                        let data1 = try? Data(contentsOf: ur!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        self.poster.image = UIImage(data: data1!)
                        self.plt.text = plot
                        self.tlt.text = self.titl as String?
                        self.ActorLabel.text = self.actor as String?
                    })
                    
                 //   DispatchQueue.main.asynchronously() {
                    
                       // self.statusLabel.text = "this does not work"
                      //  self.statusLabel.text = self.getMostRecentStatusUpdate(json) // also does not work
                   // }
                    
                    
                 //   print(self.ActorLabel.text!)
                    
                    //  let currentTemperatureF = currentConditions["temperature"] as! Double
                    //print(currentTemperatureF)
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
        
     self.updates()
       
       

    }
    
    func updates(){
       //update
        //self.poster.image = self.img.image
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        ActorLabel.isHidden = true;
       
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

